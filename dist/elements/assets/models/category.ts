export interface ICmsCategory {
    name: string;
    key: string;
    genreDefault?: string;
}



// interface ICmsChannelPerPlan {
//   type: string;
//   plan_id: string;
//   channels: ICmsChannelDetail[];
// }
//
export interface ICmsChannelDetail {
    artistsYouHear: string;
    assetId: number;
    available: string;
    availableToPackage: string;
    category: string;
    categoryIcon: string;
    categoryTitle: string;
    categoryVanityURL: string;
    channelLogo46positive: string;
    channelLogo46reverse: string;
    channelLogo76positive: string;
    channelLogo76reverse: string;
    contentId: string;
    deepLink: string;
    displayName: string;
    genre: string;
    genreLogo: string;
    genreTitle: string;
    genreVanityURL: string;
    longDescription: string;
    mediumDescription: string;
    shortDescription: string;
    siriusChannelNumber: number;
    vanityURL: string;
    xmChannelNumber: number;
}

export interface ICmsGenre {
    name: string;
    key: string;
    category: string;
}
