if (!!window.MSInputMethodContext && !!document.documentMode || document.documentMode <= 11) {
    window.MouseEvent.prototype = Event.prototype;

    window.MouseEvent = function (eventType, params) {
        params = params || {
            bubbles: false,
            cancelable: false
        };
        var mouseEvent = document.createEvent('MouseEvent');
        mouseEvent.initMouseEvent(eventType, params.bubbles, params.cancelable, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);

        return mouseEvent;
    }
}