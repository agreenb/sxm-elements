import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {ICmsCategory, ICmsChannelDetail, ICmsGenre} from '../assets/models/category';
import {CATEGRORIES} from '../assets/json/mock-categories';
import {GENRES} from '../assets/json/mock-genres';

@Injectable({
  providedIn: 'root',
})
export class DataService {



  catID = '';
  genID = '';
  private categories = new Subject<ICmsCategory[]>();
  private selectedCategory = new Subject<ICmsCategory>();

  private genres = new Subject<ICmsGenre[]>();
  private selectedGenre = new Subject<ICmsGenre>();

  private channels = new Subject<ICmsChannelDetail[]>();

  // apiUrl = 'https://www.siriusxm.com/servlet/Satellite?pagename=SXM/Utility/ChannelLineupService&pkgid=SXM_SIR_AUD_ALLACCESS&catid=sports';
  constructor(private _http: HttpClient) {}




  getChannelData(): Observable<ICmsChannelDetail[]> {
    const url = 'https://cors-anywhere.herokuapp.com/https://www.siriusxm.com/servlet/Satellite?pagename=SXM/Utility/ChannelLineupService&pkgid=SXM_SIR_AUD_ALLACCESS&catid=' + this.gcategory.key;
    return this._http
        .get<ICmsChannelData>(url).pipe(map(data => data.channels.filter(channel => channel.genre === this.genID)));
  }

  // getChannelJSON(): ICmsChannelDetail[] {
  //   const url = 'https://cors-anywhere.herokuapp.com/https://www.siriusxm.com/servlet/Satellite?pagename=SXM/Utility/ChannelLineupService&pkgid=SXM_SIR_AUD_ALLACCESS&catid=' + this.gcategory.key;
  //   const json = this._http
  //       .get<ICmsChannelData>(url)
  //       .pipe(map(data => data.channels
  //           .filter((channel: ICmsChannelDetail) => channel.genre === this.genID))).;
  // }

  updateCategory(category: ICmsCategory) {
    this.catID = category.key;
    this.selectedCategory.next(category);
    // this.updateGenres();
  }

  getCategory(): Observable<ICmsCategory>{
    return this.selectedCategory.asObservable();
  }

  updateCategories(): void {
    console.log('updateCategories');
    this.categories.next(CATEGRORIES);
  }
  getCategories(): Observable<ICmsCategory[]> {
    console.log('getCategories');
    return this.categories.asObservable();
  }

  updateGenre(genre?: ICmsGenre): void {
    console.log('updateGenre');
    this.genID = genre.key;
    this.selectedGenre.next(genre);
    this.getChannelData();
  }

  getGenre(): Observable<ICmsGenre> {
    console.log('getGenre');
    return this.selectedGenre.asObservable();
  }

  updateGenres(): void {
    console.log('updateGenres');
    this.genres.next(GENRES.filter(genre => genre.category === this.catID));
    console.log('this.genres[0] ', this.genres.asObservable()[0]);
    this.selectedGenre.next(this.genres[0]);
    console.log('this.selectedGenre: ', this.selectedGenre.asObservable());
  }

  getGenres(): Observable<ICmsGenre[]> {
    console.log('getGenres');
    return this.genres.asObservable();
  }





  getPlanPricing(url: string, programId: string): Promise<ICmsPlanPricing> {
    const funcName = 'GetPlanPricing()';

    return new Promise((resolve, reject) => {
      try {
        this._getMicroserviceData(url).subscribe(
          response => {
            const pricing: ICmsPlanPricing = {
              type: 'pricing',
              program_id: programId,
              plans: this._parsePlanPricing(response.data.plans),
            };
            resolve(pricing);
          },
          error => {
            reject(error);
          }
        );
      } catch (err) {
        reject(err);
      }
    });
  }

  private _parsePlanPricing(plans): ICmsPlanPricingDetail[] {
    const funcName = 'ParsePlanPricing()';
    const pricingDetails: ICmsPlanPricingDetail[] = [];

    plans.forEach(p => {
      if (p.id && p.name) {
        if (!p['quotes']) {
          throw new Error(`Plan Pricing: Quotes property cannot be located.`);
        }

        const quotesProp = p['quotes'];

        const quotesData = {
          current_quote: <ICmsPlanPricingQuotes>quotesProp.find(t => t.type === 'CURRENT_QUOTE'),
          future_quote: <ICmsPlanPricingQuotes>quotesProp.find(t => t.type === 'FUTURE_QUOTE'),
          renewal_quote: <ICmsPlanPricingQuotes>quotesProp.find(t => t.type === 'RENEWAL_QUOTE'),
        };

        const detail: ICmsPlanPricingDetail = {
          type: p.type,
          id: p.id,
          name: p.name,
          descriptor: p.descriptor,
          marketType: p.marketType,
          termLength: p.termLength,
          plan_id: p.plan_id,
          eligibleForMrd: p.eligibleForMrd,

          isPromotion: false,
          pricePerMonth: 0,
          msrpPerMonth: null,
          msrpTotal: null,
          upfrontQuote: null,
          echoDotValue: null,
          savingsPercentage: null,
          earlyTerminationFee: 50,

          quotes: quotesData,
        };

        detail.pricePerMonth = quotesData.current_quote.pricePerMonthLineItem.price;
        if (quotesData.future_quote) {
          detail.isPromotion = true;
          detail.msrpPerMonth = quotesData.renewal_quote.pricePerMonthLineItem.price;
          detail.msrpTotal = detail.msrpPerMonth * p.termLength;
          detail.upfrontQuote = quotesData.current_quote.total.price * p.termLength;
          detail.echoDotValue = quotesData.current_quote.total.price * p.termLength;
          detail.savingsPercentage = Math.trunc((1 - detail.upfrontQuote / detail.msrpTotal) * 100);
        }

        pricingDetails.push(detail);
      } else {
        throw new Error('CMS Json content: Invalid Plan');
      }
    });

    return pricingDetails;
  }

  private _getMicroserviceData(url: string): Observable<any> {
    return this._http.get<any>(url).pipe(catchError(this._errorHandler));
  }
  private _errorHandler(_errorHandler: any): any {
    throw new Error('Method not implemented.');
  }
}

interface ICmsPlanPricing {
  type: string;
  program_id: string;
  plans: ICmsPlanPricingDetail[];
}
//
interface ICmsChannelData {
  channels: ICmsChannelDetail[];
  mktData: any;
}
//
// interface ICmsMktData{
//   Platform: string;
//   additionalIcons: []
//   featuredIcons: (3) ["https://www.siriusxm.com/servlet/Satellite?blobcol…=ImageAsset&blobwhere=1494451558185&ssbinary=true", "https://www.siriusxm.com/servlet/Satellite?blobcol…=ImageAsset&blobwhere=1293947774072&ssbinary=true", "https://www.siriusxm.com/servlet/Satellite?blobcol…=ImageAsset&blobwhere=1293947774090&ssbinary=true"]
//   lineupvanityurl: "/packages/sxmallaccess"
//   package_msrp_monthly: "19.99"
//   packagebullets_long: "175+ Channels
//   ↵Listen Online + On the App
//   ↵Commercial-Free Music Channels
//   ↵Howard Stern
//   ↵Exclusive Artist-Dedicated Music Channels
//   ↵24/7 Comedy Channels
//   ↵Every NFL Game
//   ↵Every NASCAR® Race
//   ↵MLB®, NBA, and NHL® Games
//   ↵PGA TOUR® Coverage"
//   packagectaurl: "https://care.siriusxm.com/login_view.action?pkg=AA"
//   packageshortname: "All Access"
//   packagevanityurl: "/packages/sxmallaccess"
// }

interface ICmsPlanPricingDetail {
  type: string;
  id: string;
  name: string;
  descriptor: string;
  marketType: string;
  termLength: number;
  plan_id: string;
  eligibleForMrd: boolean;

  isPromotion: boolean;
  pricePerMonth: number;
  msrpPerMonth?: number;
  msrpTotal?: number;
  upfrontQuote?: number;
  echoDotValue?: number;
  savingsPercentage?: number;
  earlyTerminationFee?: number;

  quotes: {
    current_quote: ICmsPlanPricingQuotes;
    future_quote?: ICmsPlanPricingQuotes;
    renewal_quote: ICmsPlanPricingQuotes;
  };
}

interface ICmsPlanPricingQuotes {
  type: string;

  pricePerMonthLineItem: {
    description: string;
    price: number;
  };

  serviceQuoteLineItem: {
    description: string;
    price: number;
  };

  taxQuoteLineItem: {
    description: string;
    price: number;
  };

  feeQuoteLineItems: [
    {
      description: string;
      price: number;
    }
  ];

  total: {
    description: string;
    price: number;
  };

  startDate: number;
  endDate: number;
}
