// ===============================================================================
// Angular
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { HttpClientModule, HttpClient } from '@angular/common/http';

// =========================================================================
// Libs
import { ModalModule, TooltipModule } from 'ngx-bootstrap';

// ===============================================================================
// Modules
import { PlanComponent } from './+plans/plan.component';

@NgModule({
  declarations: [PlanComponent],
  imports: [BrowserModule, HttpClientModule, TooltipModule.forRoot()],
  providers: [],
  entryComponents: [PlanComponent],
  // Remove for Angular Element creation
  bootstrap: [PlanComponent],
})
export class AppModule {
  constructor(injector: Injector) {
    const acc = createCustomElement(PlanComponent, { injector });
    customElements.define('custom-plan', acc);
  }
  ngDoBootstrap() {}
}
