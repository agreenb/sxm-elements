import { Component, Type, ViewEncapsulation, OnInit, OnChanges } from '@angular/core';
import { DataService } from '../data.service';
import {debounce} from "rxjs/operators";

@Component({
  selector: 'custom-plan',
  templateUrl: './plan.component.html',
  styleUrls: ['./plan.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
  // encapsulation: ViewEncapsulation.ShadowDom,
})
export class PlanComponent implements OnInit, OnChanges {
  // ================================================
  // ===             Scope Properties             ===
  // ================================================
  package = [];
  offerDesc: string = null;
  dynamicPlanComponent: Type<any>;
  plans = [];
  program = '';
  channels: any[];

  constructor(private service: DataService) {
    this.service
      .getPlanPricing('http://localhost:3000/api/ms-plan/plans/newsxir/programcode', 'newsxir')
      .then(val => {
        this.program = val.program_id;
        val.plans.forEach(element => {
          const plan = {
            cardCssClass: '',
            package_name: '',
            pricePerMonth: 0,
            msrpPerMonth: 0,
            plan_descr: 'Plan Desc goes here',
            planDetail: 'Plan Details',
            isPromotion: false,
          };
          plan.cardCssClass = 'col-xs-6 col-sm-6 col-md-6 package-card';
          plan.pricePerMonth = element.pricePerMonth;
          plan.msrpPerMonth = element.msrpPerMonth;
          plan.isPromotion = element.isPromotion;
          plan.package_name = element.name;
          this.plans.push(plan);
        });
      });

    this.service.getChannelData().subscribe(response => {
      this.channels = response;
      console.log(response);
    });

  }

  ngOnInit(): void {

  }

  ngOnChanges(): void {}
}
