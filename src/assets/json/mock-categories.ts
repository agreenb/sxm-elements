
interface ICmsCategory {
  name: string;
  key: string;
  genreDefault?: string;
}

export const CATEGRORIES: ICmsCategory[] = [
  {
    'name': 'Music',
    'key': 'music'
  },
  {
    'name': 'Sports',
    'key': 'sports'
  },
  {
    'name': 'Talk & Entertainment',
    'key': 'talk'
  },
  {
    'name': 'News & Issues',
    'key': 'news'
  },
  {
    'name': 'Howard Stern',
    'key': 'howard'
  },
  {
    'name': 'All',
    'key': 'all'
  }
];
