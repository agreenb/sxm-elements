interface ICmsGenre {
  name: string;
  key: string;
  category: string;
}

export const GENRES: ICmsGenre[] = [
  {
    'name': 'Pop',
    'key': 'pop',
    'category': 'music'
  },
  {
    'name': 'Rock',
    'key': 'rock',
    'category': 'music'
  },
  {
    'name': 'Hip-Hop/R&B',
    'key': 'hiphop',
    'category': 'music'
  },
  {
    'name': 'Dance/Electronic',
    'key': 'dance',
    'category': 'music'
  },
  {
    'name': 'Country',
    'key': 'country',
    'category': 'music'
  },
  {
    'name': 'Jazz/Standards',
    'key': 'jazz',
    'category': 'music'
  },
  {
    'name': 'Classical',
    'key': 'classical',
    'category': 'music'
  },
  {
    'name': 'Christian',
    'key': 'christian',
    'category': 'music'
  },
  {
    'name': 'More',
    'key': 'more',
    'category': 'More'
  },
  {
    'name': 'Canadian',
    'key': 'canadian',
    'category': 'More'
  },
  {
    'name': 'Comedy',
    'key': 'comedy',
    'category': 'Comedy'
  },
  {
    'name': 'Politics/Issues',
    'key': 'political',
    'category': 'news'
  },
  {
    'name': 'News/Public Radiotest',
    'key': 'publicradio',
    'category': 'news'
  },
  {
    'name': 'Traffic/Weather',
    'key': 'trafficandnews',
    'category': 'news'
  },
  {
    'name': 'Howard Stern',
    'key': 'howardstern',
    'category': 'howard'
  },
  {
    'name': 'Sports Talk',
    'key': 'sportstalk',
    'category': 'sports'
  },
  {
    'name': 'NFL',
    'key': 'nflplay',
    'category': 'sports'
  },
  {
    'name': 'NBA',
    'key': 'nbaplay',
    'category': 'sports'
  },
  {
    'name': 'NHL',
    'key': 'NHL_PBP',
    'category': 'sports'
  },

  {
    'name': 'MLB',
    'key': 'mlbpbp',
    'category': 'sports'
  },
  {
    'name': 'College',
    'key': 'college',
    'category': 'sports'
  },
  {
    'name': 'Other Sports',
    'key': 'sportsplay',
    'category': 'sports'
  },
  {
    'name': 'Religion',
    'key': 'religion',
    'category': 'talk'
  },
  {
    'name': 'Family',
    'key': 'kids',
    'category': 'talk'
  },
  {
    'name': 'Entertainment',
    'key': 'entertainment',
    'category': 'talk'
  },
  {
    'name': 'All Music',
    'key': 'allmusic',
    'category': 'music'
  },
  {
    'name': 'All More',
    'key': 'allmore',
    'category': 'More'
  },
  {
    'name': 'All Comedy',
    'key': 'allcomedy',
    'category': 'Comedy'
  },
  {
    'name': 'All News & Issues',
    'key': 'allnews',
    'category': 'news'
  },
  {
    'name': 'All Howard Stern',
    'key': 'allhoward',
    'category': 'howard'
  },
  {
    'name': 'All Sports',
    'key': 'allsports',
    'category': 'sports'
  },
  {
    'name': 'All',
    'key': 'all',
    'category': 'All'
  },
  {
    'name': 'All Talk & Entertainment',
    'key': 'alltalkent',
    'category': 'talk'
  }
];
