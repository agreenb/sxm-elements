# Elements

Welcome to the wonderful world of Angular Elements!
Lets go over the basics of getting this thing up and running.

## Development server

To run the element as a regular Angular application you need to comment in the line `// bootstrap: [PlanComponent],` in `app.module.ts`. To export the component as a AE (Angular Element, going forward) make sure to comment the line out!

## Build of the AE for export

Run `npm run build` to build the component. The build artifacts will be stored in the `dist/` directory.
Once that is done, run `npm run package` and it will cat the multiple `.js` files into one and transfer it to the `public` folder.

At this point you are basically done building it for use in a html doc.
If you want to run and test it to make sure everything build right, you can run `npm run serve` this will start up http-server and host it on `http://127.0.0.1:8080`.

## Running the mock database

Run `node server.js` to start up the mock data which this component needs, this is optional in case your component uses an external API

## Changing stuff around and how to use the new element

Inside `public/index.html` you will see two things:

1. `<script src="elements.js"></script>` which pulls in the `.js` file created by the `npm run package` command

2. `<custom-plan><span slot="header">A serious header</span><span slot="details">With some crucial details</span></custom-plan>`

The second line calls in the AE and passes it some data using `slot`. The `='header'` tells it which slot to use in the AE and what is between the `<></>` is the data that will be injected.

Go ahead and play around with it!

## Still have questions?

If you have any additional questions just slack or email me at p.baranowski@bigspaceship.com
