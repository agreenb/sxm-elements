//=========================================================================
//=== ** PLEASE NOTE that this Node server code and its related backend ===
//===    code under the /server folder will be erased once the Java    ===
//===    services has been implemented.  This code is only temporary    ===
//===    To get the architecture completed.                             ===
//=========================================================================

(function () {
    'use strict';

    if (!process.env.NODE_ENV) {
        process.env.NODE_ENV = 'development';
    }

    const logPrefix = '[Router]:';
    const env = process.env.NODE_ENV;

    console.log(`${logPrefix} Load(). Node environment: ${env}`);
    console.log(`${logPrefix} Load(). Loading config.${env}.json`);

    module.exports = require(`../environment/config.${env}.json`);
})();