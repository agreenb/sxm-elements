//=========================================================================
//=== ** PLEASE NOTE that this Node server code and its related backend ===
//===    code under the /server folder will be erased once the Java     ===
//===    services has been implemented.  This code is only temporary    ===
//===    To get the architecture completed.                             ===
//=========================================================================

(function() {
    'use strict';

    const logPrefix = '[LoggerSvc]:';

    const constants = require('./common.const');

    const winston = require('winston');

    let winstonLog = null;

    console.debug(`${logPrefix} In Logger.js file.`);


    //*********************************************
    class LoggerService {

        //=========================================
        //=====          Constructor          =====
        //=========================================
        constructor() {
            const funcName = 'Constructor()';

            this._isDebugMode = (process.env.NODE_ENV === 'development');
            this.debug(`${logPrefix} ${funcName}. Environment: `, process.env.NODE_ENV);

            const winstonLogPath = require('path').join(__dirname, constants.WinstonLogPath)
            this.debug(`${logPrefix} ${funcName}. Winston log path `, winstonLogPath);

            winstonLog = winston.createLogger({
                format: winston.format.combine(
                    winston.format.splat(),
                    winston.format.simple()
                ),
                transports: [
                    new winston.transports.Console(),
                    new winston.transports.File({ filename: winstonLogPath })
                ]
            });
        }

        //================================================
        //===            Public Functions              ===
        //================================================
        error(message, ...params) {
            console.error.apply(console, arguments);
        }

        warn(message, ...params) {
            console.warn.apply(console, arguments);
        }

        info(message, ...params) {
            console.info.apply(console, arguments);
        }

        debug(message, ...params) {
            if (this._isDebugMode) {
                console.debug.apply(console, arguments);
            }
        }

        log(message, ...params) {
            if (this._isDebugMode) {
                winstonLog.info.apply(console, arguments);
            }
        }
    }


    module.exports = new LoggerService();
})();