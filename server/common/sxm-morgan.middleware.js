//=========================================================================
//=== ** PLEASE NOTE that this Node server code and its related backend ===
//===    code under the /server folder will be erased once the Java     ===
//===    services has been implemented.  This code is only temporary    ===
//===    To get the architecture completed.                             ===
//=========================================================================

(function() {
    'use strict';

    const logPrefix = '[AzureMorganMW]:';

    const morgan = require('morgan'),
          stream = require('stream'),
          carrier = require('carrier'),
          uuidv4 = require('uuid/v4'),
          passStream = new stream.PassThrough();

    const azure = require('azure-storage'),
          devStoreCreds = azure.generateDevelopmentStorageCredentials(),
          tableSvc = azure.createTableService(devStoreCreds);

    console.log(`${logPrefix} In Azure-Morgan.middleware.js file.`);


    //*********************************************
    function AzureMorgan(tableName, options) {
        // Filter the arguments
        const args = Array.prototype.slice.call(arguments);

        if (args.length == 0 || !tableName) {
            throw new Error('TableName string is Null or Empty.');
        }

        if (args.length > 1 && typeof options !== 'object') {
            throw new Error('Options parameter needs to be an object. You can specify empty object like {}.');
        }

        options = options || {};

        const table = tableName || 'MorganLogs';
        const partitionKey = require('os').hostname() + ':' + process.pid;

        tableSvc.createTableIfNotExists(table, (error, result) => {
            if (error) {
                console.error(`${logPrefix} AzureMorgan(). Azure Table error: `, error);
                throw error;
            }

            console.log(`${logPrefix} AzureMorgan(). Azure Table %s is now available.`, tableName);
        });

        // Create stream to read from
        const lineStream = carrier.carry(passStream);
        lineStream.on('line', onLine);

        // Morgan options stream
        options.stream = passStream;

        function onLine(line) {
            let entity = {
                PartitionKey: partitionKey,
                RowKey: uuidv4(),

                ResponseTime: '',
                Method: '',
                Url: '',
                ResponseLength: '',
                ResponseStatus: '',

                Referrer: '',
                UserAgent: '',
                RemoteAddr: '',
                RemoteUser: '',
                HttpVersion: '',

                FullLog: line
            };

            try {
                if (line) {
                    const log = String(line).split('|');

                    entity.RemoteAddr = log[0];
                    entity.RemoteUser = log[1];
                    entity.Method = log[3];
                    entity.Url = log[4];
                    entity.HttpVersion = log[5];
                    entity.ResponseStatus = log[6];
                    entity.ResponseLength = log[7];
                    entity.Referrer = log[8];
                    entity.UserAgent = log[9];
                    entity.ResponseTime = log[10];
                }
            }
            catch (error) {
                console.error(`${logPrefix} OnLine(). Azure InsertEntity error: `, error);
            }

            tableSvc.insertEntity(table, entity,
                (error, result) => {
                    if (error) {
                        console.error(`${logPrefix} OnLine(). Azure InsertEntity error: `, error);
                        throw error;
                    }
                });
        }

        const format = ':remote-addr|:remote-user|[:date[clf]]|:method|":url"|HTTP/:http-version|:status|:res[content-length]|":referrer"|":user-agent"|:response-time ms';
        var azureMorgan = morgan(format, options);
        return azureMorgan;
    }

    module.exports = AzureMorgan;
    module.exports.compile = morgan.compile
    module.exports.format = morgan.format
    module.exports.token = morgan.token
})();