//=========================================================================
//=== ** PLEASE NOTE that this Node server code and its related backend ===
//===    code under the /server folder will be erased once the Java     ===
//===    services has been implemented.  This code is only temporary    ===
//===    To get the architecture completed.                             ===
//=========================================================================

(function () {
    'use strict';

    class ContentEndpointModel {
        constructor() {
            this.id = null;

            this.contentId = null;
            this.contentUrl = null;
        }
    }

    module.exports = ContentEndpointModel;
})();