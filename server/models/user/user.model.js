//=========================================================================
//=== ** PLEASE NOTE that this Node server code and its related backend ===
//===    code under the /server folder will be erased once the Java     ===
//===    services has been implemented.  This code is only temporary    ===
//===    To get the architecture completed.                             ===
//=========================================================================

(function () {
    'use strict';

    //const baseModel = require('../base/base.model');

    class UserModel {
        constructor() {
            this.id = Math.floor(Math.random() * 16) + 5;

            this.firstName = null;
            this.lastName = null;
            this.email = null;
            this.password = null;
            this.rememberMe = false;

            this.ipAddress = null;
            this.createdAt = new Date();
            this.updatedAt = null;
        }
    }

    module.exports = UserModel;
})();