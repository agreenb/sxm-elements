//=========================================================================
//=== ** PLEASE NOTE that this Node server code and its related backend ===
//===    code under the /server folder will be erased once the Java     ===
//===    services has been implemented.  This code is only temporary    ===
//===    To get the architecture completed.                             ===
//=========================================================================

(function () {
    'use strict';

    //const baseModel = require('../base/base.model');

    class PlanModel {
        constructor() {
            this.id = Math.floor(Math.random() * 16) + 5;

            this.planName = null;
            this.offerTitle = null;
            this.planExclusiveness = null;
            this.offerDetailsTitle = null;

            this.price = '0';
            this.regularPrice = null;
            this.terms = 'mo';
            this.duration = '';

            this.offerDetails = {};

            // Promotion
            this.promoFeesAndTaxes = '';
            this.promoMusicRoyaltyFee = '';
            this.promoLocalTaxes = '';
            this.promoTotalDueToday = '';

            // Renewal
            this.renewalMonthlyPlan = '';
            this.renewalMusicRoyaltyFee = '';
            this.renewalLocalTaxes = '';
            this.renewalCreditAndAjdustments = '';
            this.renewalTotalDueToday = '';
            this.renewalDueDate = '';

            this.details = [];

            this.isActive = true;

            this.promoStartDate = new Date();
            this.promoEndDate = (new Date()).getDate() + 75;

            this.createdAt = new Date();
            this.updatedAt = null;
        }
    }

    module.exports = PlanModel;
})();