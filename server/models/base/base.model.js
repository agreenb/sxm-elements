//=========================================================================
//=== ** PLEASE NOTE that this Node server code and its related backend ===
//===    code under the /server folder will be erased once the Java     ===
//===    services has been implemented.  This code is only temporary    ===
//===    To get the architecture completed.                             ===
//=========================================================================

(function () {
    'use strict';

    class BaseModel {
        constructor() {
            this.id = Math.floor(Math.random() * 16) + 5;;
            this.ipAddress = null;
            this.createdAt = new Date();
            this.updatedAt = null;
        }
    }

    module.exports = BaseModel;
})();