//=========================================================================
//=== ** PLEASE NOTE that this Node server code and its related backend ===
//===    code under the /server folder will be erased once the Java     ===
//===    services has been implemented.  This code is only temporary    ===
//===    To get the architecture completed.                             ===
//=========================================================================

(function() {
    'use strict';

    const logPrefix = '[BaseRepo]:';

    const logger = require('../common/logger.service');


    //*********************************************
    class BaseRepository {

        //=========================================
        //=====          Constructor          =====
        //=========================================
        constructor() {
            //logger.debug(logPrefix, 'Constructor()', '_Application_', `In function.`);
        }

        //===============================================
        //=====           Public Functions          =====
        //===============================================
        getUrlData(url) {
            const funcName = 'GetUrlData()';
            logger.debug(`${logPrefix} ${funcName}. Url param: `, url);

            return new Promise((resolve, reject) => {
                const http = require('http'),
                      https = require('https');

                let client = http;

                if (url.toString().indexOf('https') === 0) {
                    client = https;
                }

                client
                    .get(url, (resp) => {
                        let data = '';

                        // A chunk of data has been recieved.
                        resp.on('data', (chunk) => {
                            data += chunk;
                        });

                        // The whole response has been received. Print out the result.
                        resp.on('end', () => {
                            resolve(data);
                        });
                    })
                    .on('error', (err) => {
                        logger.error(`${logPrefix} ${funcName}. Error: `, err);

                        reject(err);
                    });
            });
        }
    }


    module.exports = BaseRepository;
})();