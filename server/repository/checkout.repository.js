//=========================================================================
//=== ** PLEASE NOTE that this Node server code and its related backend ===
//===    code under the /server folder will be erased once the Java     ===
//===    services has been implemented.  This code is only temporary    ===
//===    To get the architecture completed.                             ===
//=========================================================================

(function () {
    'use strict';

    const logPrefix = '[CheckoutRepository]:';

    const logger = require('../common/logger.service');

    //************************************************
    class CheckoutRepository {

        //=========================================
        //=====      Checkout Functions        =====
        //=========================================
        Checkout_VerifyPin(pinNumber, callback) {
            const funcName = 'Checkout_VerifyPin()';
            logger.debug(`${logPrefix} ${funcName}. Pin number param: `, pinNumber);

            callback(null, true);
        }
    }

    module.exports = new CheckoutRepository();
})();