//=========================================================================
//=== ** PLEASE NOTE that this Node server code and its related backend ===
//===    code under the /server folder will be erased once the Java     ===
//===    services has been implemented.  This code is only temporary    ===
//===    To get the architecture completed.                             ===
//=========================================================================

(function() {
    'use strict';

    const logPrefix = '[MsPlanRepository]:';

    const logger = require('../common/logger.service'),
          constants = require('../common/common.const');


    //************************************************
    class MsPlanRepository {

        //=========================================
        //=====        Public Functions       =====
        //=========================================
        Plan_GetAllById(programId, codeTypeId) {
            const funcName = 'Plan_GetAllById()';
            logger.debug(`${logPrefix} ${funcName}. Params.  ProgramId: %s.  CodeTypeId: `, programId, codeTypeId);

            return new Promise((resolve, reject) => {
                try {
                    let typeId = this._getPlanCodeType(codeTypeId),
                        quotesModel = this._getPlanPricingModel(programId, typeId);

                    if (!quotesModel) {
                        logger.error(`${logPrefix} ${funcName}. Default ESN Plan Quotes used. Missing Json for type: `, codeTypeId);

                        typeId = constants.DefaultCodeType;
                        quotesModel = this._getPlanPricingModel(programId, typeId);

                        if (!codeTypeId) {
                            throw new Error(`Unable to load Plan Quotes Model based on CodeTypeId: ${typeId}`);
                        }
                    }

                    return resolve(quotesModel);
                }
                catch (err) {
                    logger.error(`${logPrefix} ${funcName}. Error: `, err);
                    return reject(`Failed to load Microservice Plan Quotes.  Error: ${err.message}`);
                }
            });
        }

        //===============================================
        //=====           Helper Functions          =====
        //===============================================
        _getPlanCodeType(codeTypeId) {
            if (!codeTypeId) {
                throw new Error(`CodeTypeId parameter cannot be Null or Undefined`);
            }

            const typeId = String(codeTypeId).toLowerCase().replace(' ', '').replace('-', '');
            if (constants.CodeTypes.includes(typeId)) {
                return typeId;
            }

            logger.error(`${logPrefix} GetPlanCodeType(). Default Plan Code TypeID used. Invalid CodeTypeId: `, codeTypeId);
            return constants.DefaultCodeType;
        }

        _getPlanPricingModel(programId, codeTypeId) {
            if (!codeTypeId) {
                throw new Error(`CodeTypeId parameter cannot be Null or Undefined`);
            }

            return require(`${constants.MicroservicesDataPath}/${programId}/plans-quotes-${codeTypeId}.json`);
        }
    }


    module.exports = new MsPlanRepository();
})();