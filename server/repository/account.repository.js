//=========================================================================
//=== ** PLEASE NOTE that this Node server code and its related backend ===
//===    code under the /server folder will be erased once the Java     ===
//===    services has been implemented.  This code is only temporary    ===
//===    To get the architecture completed.                             ===
//=========================================================================

(function() {
    'use strict';

    const logPrefix = '[AccountRepository]:';

    const logger = require('../common/logger.service');


    //************************************************
    class AccountRepository {

        //=========================================
        //=====      Account Functions        =====
        //=========================================
        Account_Register(model) {
            const funcName = 'User_Insert()';
            logger.debug(`${logPrefix} ${funcName}. Model param: `, model);

            return new Promise((resolve, reject) => {
                try {
                    const retModel = Object.assign({}, model);
                    retModel.id = Math.floor(Math.random() * 16) + 5;

                    logger.debug(`${logPrefix} ${funcName}. User registered.  New Model Id: `, retModel._id);
                    return resolve(model);
                }
                catch (err) {
                    logger.error(`${logPrefix} ${funcName}. Error: `, err);
                    return reject(`Error: ${err.message}`);
                }
            });
        }

        Account_Login(email) {
            const funcName = 'Account_Login()';
            logger.debug(`${logPrefix} ${funcName}. Email param: `, email);

            return new Promise((resolve, reject) => {
                try {
                    const retModel = { email: email };

                    logger.debug(`${logPrefix} ${funcName}. Model retrieved.`);
                    return resolve(retModel);
                }
                catch (err) {
                    logger.error(`${logPrefix} ${funcName}. Error: `, err);
                    return reject(`Error: ${err.message}`);
                }
            });
        }
    }


    module.exports = new AccountRepository();
})();