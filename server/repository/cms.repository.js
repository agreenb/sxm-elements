//=========================================================================
//=== ** PLEASE NOTE that this Node server code and its related backend ===
//===    code under the /server folder will be erased once the Java     ===
//===    services has been implemented.  This code is only temporary    ===
//===    To get the architecture completed.                             ===
//=========================================================================

(function() {
    'use strict';

    const logPrefix = '[CmsRepository]:';

    const baseRepo = require('./base.repository');

    const logger = require('../common/logger.service'),
          constants = require('../common/common.const');


    //*********************************************
    class CmsRepository extends baseRepo {

        //=========================================
        //=====          CMS Functions        =====
        //=========================================
        Cms_GetById(contentId, programId) {
            const funcName = 'Cms_GetById()';
            logger.debug(`${logPrefix} ${funcName}. Params.  ContentId: %s.  ProgramId: `, contentId, programId);

            return new Promise((resolve, reject) => {
                try {
                    const endpointsData = require(`${constants.CmsDataPath}/content-endpoints.json`);

                    logger.debug(`${logPrefix} ${funcName}. CMS endpoints: `, endpointsData);

                    if (endpointsData && Array.isArray(endpointsData)) {
                        if (endpointsData.length === 0) {
                            throw new Error('CMS Content Endpoint Model is empty');
                        }

                        const endpoint = endpointsData.filter(l => l.contentId === contentId && l.program_id === programId);
                        if (!endpoint) {
                            throw new Error(`Unable to locate ContentId in the CMS Content Endpoints: ${contentId}`);
                        }
                        logger.debug(`${logPrefix} ${funcName}. Located endpoint data: `, endpoint);

                        let endpointModel = require('../models/cms/content-endpoint.model');
                        if (!endpointModel) {
                            throw new Error(`Failed to load Json Data into CMS Content Endpoint Model: ${contentId}`);
                        }
                        endpointModel = endpoint[0];

                        const fromFile = (endpointModel.getFromJsonFile);
                        if (fromFile) {
                            const json = require(endpointModel.contentUrlFile);
                            logger.debug(`${logPrefix} ${funcName}. Loaded CMS content from Json file: `, json);

                            return resolve(json);
                        }

                        this.getUrlData(endpointModel.contentUrl)
                            .then((response) => {
                                logger.debug(`${logPrefix} ${funcName}. Loaded CMS content from Url: `, response);

                                const json = JSON.parse(response);
                                return resolve(json);
                            })
                            .catch((err) => {
                                logger.error(`${logPrefix} ${funcName}. Error: `, err);
                                return reject(`Error: ${err.message}`);
                            });
                    }
                }
                catch (err) {
                    logger.error(`${logPrefix} ${funcName}. Error: `, err);
                    return reject(`Failed to load CMS endpoints.  Error: ${err.message}`);
                }
            });
        }
    }


    module.exports = new CmsRepository();
})();