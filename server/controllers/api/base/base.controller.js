//=========================================================================
//=== ** PLEASE NOTE that this Node server code and its related backend ===
//===    code under the /server folder will be erased once the Java     ===
//===    services has been implemented.  This code is only temporary    ===
//===    To get the architecture completed.                             ===
//=========================================================================

(function() {
    'use strict';

    const logPrefix = '[BaseCtrl]:';

    const constants = require('../../../common/common.const'),
          logger = require('../../../common/logger.service');


    //*********************************************
    class BaseController {

        //=========================================
        //=====          Constructor          =====
        //=========================================
        constructor() {
            //logger.debug(`${logPrefix} Constructor(). In function.`);
        }

        //===============================================
        //=====         Protected Functions         =====
        //===============================================
        getUrlFragmentId(req, res, funcName, idName) {
            let id = null;

            try {
                this.validateParameter(req.body, 'Req');

                id = req.params[idName];

                logger.debug(`${logPrefix} ${funcName}. ${idName}:`, id);
                this.validateParameter(id, idName);
            }
            catch (err) {
                logger.debug(`${logPrefix} ${funcName}. Error:`, err);  // Debug OK

                res.status(constants.ResCodes.BAD_REQUEST)
                   .json(this.payload(constants.ResCodes.BAD_REQUEST, err.message, null, funcName));
            }

            return id;
        }

        validateParameter(param, paramName, funcName) {
            if (!param) {
                throw new Error(`${paramName} cannot be Null or Undefined`);
            }
        }

        payload(status, message, data, funcName) {
            return {
                status: status,
                message: (message === null) ? null : `${funcName}. ${message}.`,
                data: data
            }
        }

        sendResponse(funcName, err, retModel, res, errMsg, onlyPartial) {
            if (err) {
                logger.debug(`${logPrefix} ${funcName}. Error:`, err);

                let message = err;
                if (err.hasOwnProperty('message')) {
                    message = err.message;
                }

                res.status(constants.ResCodes.NOT_FOUND)
                   .json(this.payload(constants.ResCodes.NOT_FOUND, `${errMsg}. ${message}`, null, funcName));

                return false;
            }

            logger.debug(`${logPrefix} ${funcName}. Returned model:`, retModel);

            if (retModel === null) {
                res.status(constants.ResCodes.NOT_FOUND)
                   .send(this.payload(constants.ResCodes.NOT_FOUND, 'Data was not found', null, funcName));

                return false;
            }

            if (onlyPartial) {
                return true;
            }

            res.status(constants.ResCodes.OK)
               .json(this.payload(constants.ResCodes.OK, null, retModel, funcName));
        }

        sendResponse_Err(funcName, err, res, errMsg) {
            if (err) {
                if (typeof err === 'string') {
                    res.status(constants.ResCodes.NOT_FOUND)
                       .json(this.payload(constants.ResCodes.NOT_FOUND, `${errMsg}. ${err}`, null, funcName));

                   return false;
                }

                logger.error(`${logPrefix} ${funcName}. Error:`, err);

                let message = err;
                if (err.hasOwnProperty('message')) {
                    message = err.message;
                }

                res.status(constants.ResCodes.INTERNAL_SERVER_ERROR)
                   .json(this.payload(constants.ResCodes.INTERNAL_SERVER_ERROR, message, null, funcName));

                return false;
            }
        }

        sendResponse_Model(funcName, retModel, res, onlyPartial) {
            logger.debug(`${logPrefix} ${funcName}. Returned model:`, retModel);

            if (retModel === null) {
                res.status(constants.ResCodes.NOT_FOUND)
                   .send(this.payload(constants.ResCodes.NOT_FOUND, 'Data was not found', null, funcName));

                return false;
            }

            if (onlyPartial) {
                return true;
            }

            res.status(constants.ResCodes.OK)
               .json(this.payload(constants.ResCodes.OK, '', retModel, funcName));
        }
    }


    module.exports = BaseController;
})();