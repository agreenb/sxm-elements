//=========================================================================
//=== ** PLEASE NOTE that this Node server code and its related backend ===
//===    code under the /server folder will be erased once the Java     ===
//===    services has been implemented.  This code is only temporary    ===
//===    To get the architecture completed.                             ===
//=========================================================================

(function () {
    'use strict';

    const logPrefix = '[MsPlanCtrl]:';

    const baseCtrl = require('../base/base.controller'),
          msPlanRepo = require('../../../repository/ms-plan.repository');

    const logger = require('../../../common/logger.service');


    //************************************************
    class MsPlanController extends baseCtrl {

        //=========================================
        //=====          Constructor          =====
        //=========================================
        constructor(router) {
            super();

            // Routes: Plan (Microservices)
            router.get('/plans/:programId/:typeId', this.Plan_GetAllById.bind(this));
        }

        //=========================================
        //=====      Plan Offer Functions     =====
        //=========================================
        Plan_GetAllById(req, res) {
            const funcName = 'Plan_GetAllById()';
            logger.debug(`${logPrefix} ${funcName}. Req param: `, req.body);

            const programId = this.getUrlFragmentId(req, res, funcName, 'programId');
            if (!programId) return;

            /****************************************************
             * Type IDs:                                        *
             *      programcode:    Program Code (Optional)     *
             *      olpt:           OLPT (Optional)             *
             *      esn:            ESN (Optional)              *
             *      singleusecode:  Single Use Code (Optional)  *
             *      planoveride:    Plan Override (Optional)    *
            *****************************************************/
            const typeId = this.getUrlFragmentId(req, res, funcName, 'typeId');
            if (!typeId) return;

            msPlanRepo
                .Plan_GetAllById(programId, typeId)
                .then((response) => {
                    return this.sendResponse_Model(funcName, response, res, false);
                })
                .catch((err) => {
                    return this.sendResponse_Err(funcName, err, res, 'Retrieval failed');
                });
        }
    }


    module.exports = MsPlanController;
})();