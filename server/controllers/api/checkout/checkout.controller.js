//=========================================================================
//=== ** PLEASE NOTE that this Node server code and its related backend ===
//===    code under the /server folder will be erased once the Java     ===
//===    services has been implemented.  This code is only temporary    ===
//===    To get the architecture completed.                             ===
//=========================================================================

(function () {
    'use strict';

    const logPrefix = '[CheckoutCtrl]:';

    const checkoutRepo = require('../../../repository/checkout.repository');

    const logger = require('../../../common/logger.service'),
          constants = require('../../../common/common.const');

    const UserModel = require('../../../models/user/user.model')


    //*********************************************
    class CheckoutController {

        //=========================================
        //=====          Constructor          =====
        //=========================================
        constructor(router) {

            // Routes: Checkout
            router.post('/verifypin/:pin', this.Checkout_VerifyPin.bind(this));
        }

        //=========================================
        //=====      Checkout Functions       =====
        //=========================================
        Checkout_VerifyPin(req, res) {
            const funcName = 'Checkout_VerifyPin()';
            logger.debug(`${logPrefix} ${funcName}. Req param: `, req.body);

            let pinNumber = null;

            try {
                _validateParameter(req.body, 'Req', funcName);

                pinNumber = req.params.pin;
                _validateParameter(pinNumber, 'Pin Number', funcName);

                if (pinNumber.toLowerCase().trim() === '1111') {
                    throw new Error('Invalid Pin')
                }
            }
            catch (err) {
                logger.error(`${logPrefix} ${funcName}. Error: `, err);

                let code = constants.ResCodes.BAD_REQUEST;
                if (err.message === 'Invalid Pin') {
                    code = constants.ResCodes.CONFLICT;
                }

                res.status(code)
                   .json(_payload(code, err.message, null, funcName));

                return;
            }

            checkoutRepo.Checkout_VerifyPin(req.body.pinNumber,
                (err, result) => {
                    if (err) {
                        logger.error(`${logPrefix} ${funcName}. Error: `, err);

                        res
                            .status(constants.ResCodes.NOT_FOUND)
                            .send(_payload(constants.ResCodes.INTERNAL_SERVER_ERROR, 'Retrieval failed', null, funcName));

                        return;
                    }

                    res.status(constants.ResCodes.OK)
                       .json(_payload(constants.ResCodes.OK, null, result, funcName));
                });
            }
    }

    //===============================================
    //=====          Private Functions          =====
    //===============================================
    function _validateParameter(param, paramName, funcName) {
        if (typeof param === 'undefined' || param === null) {
            throw new Error(`${paramName} cannot be Null or Undefined`);
        }
    }

    function _payload(status, message, data, funcName) {
        return {
            status: status,
            message: (message === null) ? null: `${funcName}. ${message}.`,
            data: data
        }
    }

    function _sendResponse(funcName, err, retModel, res, errMsg, onlyPartial){
        if (err) {
            logger.error(`${logPrefix} ${funcName}. Error: `, err);

            res.status(constants.ResCodes.NOT_FOUND)
               .json(_payload(constants.ResCodes.NOT_FOUND, errMsg, null, funcName));

            return false;
        }

        logger.debug(`${logPrefix} ${funcName}. Returned model: `, retModel);

        if (retModel === null) {
            res.status(constants.ResCodes.NOT_FOUND)
               .send(_payload(constants.ResCodes.NOT_FOUND, 'Data was not found', null, funcName));

            return false;
        }

        if (onlyPartial) {
            return true;
        }

        res.status(constants.ResCodes.OK)
           .json(_payload(constants.ResCodes.OK, null, retModel, funcName));
    }


    module.exports = CheckoutController;
})();