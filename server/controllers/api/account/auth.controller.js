//=========================================================================
//=== ** PLEASE NOTE that this Node server code and its related backend ===
//===    code under the /server folder will be erased once the Java     ===
//===    services has been implemented.  This code is only temporary    ===
//===    To get the architecture completed.                             ===
//=========================================================================

(function () {
    'use strict';

    const logPrefix = '[AccountCtrl]:';

    const baseCtrl = require('../base/base.controller'),
          accountRepo = require('../../../repository/account.repository');

    const UserModel = require('../../../models/user/user.model')

    const logger = require('../../../common/logger.service'),
          constants = require('../../../common/common.const'),
          bcryptCreds = require('../../../configs/credential-bcrypt');

    const jwt = require('jwt-simple'),
          bcrypt = require('bcrypt-nodejs');


    //*********************************************
    class UserController extends baseCtrl {

        //=========================================
        //=====          Constructor          =====
        //=========================================
        constructor(router) {
            super();

            // Routes: Account
            router.post('/register/', this.Account_Register.bind(this));
        }

        //=========================================
        //=====       Public Functions        =====
        //=========================================
        Account_Register(req, res) {
            const funcName = 'Account_Register()';
            logger.debug(`${logPrefix} ${funcName}. Req param: `, req.body);

            let model = null;

            try {
                this.validateParameter(req.body, 'Req', funcName);

                // Just used to demo purpose ONLY
                if (req.body.email.toLowerCase().trim() === 'james@xm.com') {
                    throw new Error('User already registered')
                }

                model = this._populateModel(req, 'UserRegister');
            }
            catch (err) {
                logger.error(`${logPrefix} ${funcName}. Error: `, err);

                let code = constants.ResCodes.BAD_REQUEST;
                if (err.message === 'User already registered') {
                    code = constants.ResCodes.CONFLICT;
                }

                res.status(code)
                   .json(this.payload(code, err.message, null, funcName));

                return;
            }

            bcrypt.hash(model.password, null, null,
                (err, hash) => {
                    if (err) {
                        logger.error(`${logPrefix} ${funcName}. Error: `, err);

                        res.status(constants.ResCodes.BAD_REQUEST)
                           .json(this.payload(constants.ResCodes.BAD_REQUEST, err.message, null, funcName));

                        return;
                    }

                model.password = hash;

                accountRepo
                    .Account_Register(model)
                    .then((response) => {
                        const payload = {
                            model: response,
                            token: this._createSendToken(response)
                        }

                        return payload;
                    })
                    .then((response) => {
                        return this.sendResponse_Model(funcName, response, res, false);
                    })
                    .catch((err) => {
                        return this.sendResponse_Err(funcName, err, res, 'User registration failed');
                    });
            })
        }

        Account_Login(req, res) {
            const funcName = 'Account_Login()';
            logger.debug(`${logPrefix} ${funcName}. Req param: `, req.body);

            try {
                this.validateParameter(req.body, 'Req', funcName);
            }
            catch (err) {
                logger.error(`${logPrefix} ${funcName}. Error: `, err);

                res.status(constants.ResCodes.BAD_REQUEST)
                   .json(this.payload(constants.ResCodes.BAD_REQUEST, err.message, null, funcName));

                return;
            }

            let model = this._populateModel(req, 'Userlogin');

            accountRepo
                .Account_Login(model)
                .then((response) => {
                    logger.debug(`${logPrefix} ${funcName}. Returned model: `, response);

                    bcrypt.compare(response.password, req.body.password,
                        (err, isMatch) => {
                            if (!isMatch) {
                                res.status(constants.ResCodes.UNAUTHORIZED)
                                    .json(this.payload(constants.ResCodes.UNAUTHORIZED, 'Email or Password invalid', null, funcName));

                                logger.debug(`${logPrefix} ${funcName}. Email or Password invalid`, userResponse.id);
                                return false;
                            }
                        })

                    let payload = {
                        model: response,
                        token: _createSendToken(response)
                    };

                    res.status(constants.ResCodes.OK)
                       .json(this.payload(constants.ResCodes.OK, null, payload, funcName));
                })
                .catch((err) => {
                    return this.sendResponse_Err(funcName, err, res, 'User login failed');
                });
        }

        //===============================================
        //=====           Helper Functions          =====
        //===============================================
        _populateModel(req, modelName) {
            if (modelName === 'UserRegister') {
                let model = new UserModel();

                model.firstName = req.body.firstName;
                model.lastName = req.body.lastName;
                model.email = req.body.email;
                model.password = req.body.password;
                model.rememberMe = req.body.rememberMe;
                model.ipAddress = model.ipAddress;

                return model;
            }

            else if (modelName === 'UserLogin') {
                let model = new UserModel();

                model.email = req.body.email;
                model.password = req.body.password;

                return model;
            }
        }

        _createSendToken(model) {
            const payload = { sub: model.id },
                  token = jwt.encode(payload, bcryptCreds.password);

            return token;
        }

        _checkAuthenticated(req, res, next) {
            if (!req.header('authorization')) {
                return res.status(401).send({ message: 'Unauthorized. Missing Auth Header' })
            }

            var token = req.header('authorization').split(' ')[1];

            var payload = jwt.decode(token, bcryptCreds.password);

            if (!payload) {
                return res.status(401).send({ message: 'Unauthorized. Auth Header Invalid' });
            }

            req.userId = payload.sub;

            next();
        }
    }


    module.exports = UserController;
})();