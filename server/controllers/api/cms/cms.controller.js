//=========================================================================
//=== ** PLEASE NOTE that this Node server code and its related backend ===
//===    code under the /server folder will be erased once the Java     ===
//===    services has been implemented.  This code is only temporary    ===
//===    To get the architecture completed.                             ===
//=========================================================================

(function () {
    'use strict';

    const logPrefix = '[CmsCtrl]:';

    const baseCtrl = require('../base/base.controller'),
          packageRepo = require('../../../repository/cms.repository');

    const logger = require('../../../common/logger.service');


    //*********************************************
    class CmsController extends baseCtrl {

        //=========================================
        //=====          Constructor          =====
        //=========================================
        constructor(router) {
            super();

            // Routes: CMS
            router.get('/restful/:contentId/:programId', this.Cms_GetById.bind(this));
        }

        //=========================================
        //=====        Public Functions       =====
        //=========================================
        Cms_GetById(req, res) {
            const funcName = 'Cms_GetById()';
            logger.debug(`${logPrefix} ${funcName}. Req param: `, req.body);

            const contentId = this.getUrlFragmentId(req, res, funcName, 'contentId');
            if (!contentId) return;

            const programId = this.getUrlFragmentId(req, res, funcName, 'programId');
            if (!programId) return;

            packageRepo
                .Cms_GetById(contentId, programId)
                .then((response) => {
                    return this.sendResponse_Model(funcName, response, res, false);
                })
                .catch((err) => {
                    return this.sendResponse_Err(funcName, err, res, 'Retrieval failed');
                });
        }
    }


    module.exports = CmsController;
})();