//=========================================================================
//=== ** PLEASE NOTE that this Node server code and its related backend ===
//===    code under the /server folder will be erased once the Java     ===
//===    services has been implemented.  This code is only temporaray   ===
//===    To get the architecture completed.                             ===
//=========================================================================

(function () {
    'use strict';

    const express = require('express'),
          exphbs = require('express-handlebars'),
          hbsLayouts = require('handlebars-layouts'),
          bodyParser = require('body-parser'),
          cookieParser = require('cookie-parser'),
          errorhandler = require('errorhandler'),
          //csrf = require('csurf'),
          morgan = require('morgan'),
          router = require('./server/routes/router'),
          dbConfig = require('./server/configs/config-loader').databaseConfig,
          cors = require('cors'),
          app = express(),
          port = 3000;

    const logPrefix = '[Server]:';

    console.log(`${logPrefix} In Server.js file.`);

    class Server {

        constructor() {
            console.log(`${logPrefix} Constructor(). In function.`);

            this.initViewEngine();
            this.initExpressMiddleWare();
            this.initCustomMiddleware();
            this.initRoutes();
            this.start();
        }

        start() {
            console.log(`\n${logPrefix} Start(). Starting port: `, port);

            app.listen(port, (err) => {
                console.log(`${logPrefix} Start(). [%s] Listening on http://localhost:%d: `, process.env.NODE_ENV, port);
            });
        }

        initViewEngine() {
            console.log(`${logPrefix} InitViewEngine(). Initializing view engine.`);

            const hbs = exphbs.create({
                extname: '.hbs',
                defaultLayout: 'master'
            });

            app.engine('hbs', hbs.engine);
            app.set('view engine', 'hbs');

            hbsLayouts.register(hbs.handlebars, {});
        }

        initExpressMiddleWare() {
            console.log(`${logPrefix} InitExpressMiddleWare(). Initializing Express MiddleWare.`);

            app.use(express.static(__dirname));
            app.use(morgan('dev'));
            app.use(cors());
            app.use(bodyParser.urlencoded({
                extended: true
            }));
            app.use(bodyParser.json());
            app.use(errorhandler());
            app.use(cookieParser());
            //app.use(csrf({ cookie: true }));

            // app.use((req, res, next) => {
            //     let csrfToken = req.csrfToken();

            //     res.locals._csrf = csrfToken;
            //     res.cookie('XSRF-TOKEN', csrfToken);

            //     next();
            // });

            process.on('uncaughtException', (err) => {
                if (err) {
                    console.error(`${logPrefix} InitExpressMiddleWare(). Error: `, err, err.stack);
                }
            });
        }

        initCustomMiddleware() {
            console.log(`${logPrefix} InitCustomMiddleware(). Initializing custom Middleware.`);

            if (process.platform === 'win32') {
                require('readline')
                    .createInterface({
                        input: process.stdin,
                        output: process.stdout
                    }).on('SIGINT', () => {
                        console.log(`${logPrefix} InitCustomMiddleware(). SIGINT: Closing MongoDB connection.`);
                    });
            }

            process.on('SIGINT', () => {
                console.log(`${logPrefix} InitCustomMiddleware(). SIGINT: Closing MongoDB connection.`);
            });
        }

        initRoutes() {
            console.log(`${logPrefix} InitRoutes(). Initializing Node server rountes.`);

            router.load(app, './server/controllers');

            // // redirect all others to the index (HTML5 history)
            // app.all('/*', (req, res) => {
            //     res.sendFile(__dirname + '/src/index.html');
            // });
        }
    }

    let server = new Server();
})();